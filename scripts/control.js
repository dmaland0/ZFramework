zFrameDebug = true;
zAlert.initialize(false);

var control = new zFrame('control');

controlState = control.stateObject

control.alignText = function () {
  control.Text.style.marginLeft = "";
  var headingX = control.Heading.getBoundingClientRect().x;
  var introX = control.Text.getBoundingClientRect().x;
  var difference = headingX - introX;
  control.Text.style.marginLeft = difference + "px";
};

control.validateInputs = function validate (event) {
  
  if (u.emailSeemsValid(event.target.value)) {
    controlState.IsEmail = "This looks like a real email address.";
  } else {
    controlState.IsEmail = "This doesn't seem to be a real email address.";
  }

  control.syncState()

};

control.Wrapper.widthFix = function() {
  
  control.Wrapper.style.minWidth = null;
  control.Wrapper.style.maxWidth = control.Wrapper.style.minWidth;

  control.Wrapper.style.minWidth = control.Wrapper.getBoundingClientRect().width + "px";
  control.Wrapper.style.maxWidth = control.Wrapper.style.minWidth;

};

control.APIButton.onclick = function(event) {

  var now = new Date();
  var month = now.getMonth() + 1;
  var day = now.getDate();
  var url = "http://numbersapi.com/$month/$day/date";
  url = url.replace("$month", month);
  url = url.replace("$day", day);

  zRequest('GET', url, null, [], null, false, function(xhr) {
    
    var code = "Status Code: " + xhr.status

    if (xhr.status !== 200) {

      zAlert.show("NumbersAPI zRequest Failed", "./images/failureIcon.png", code);
      controlState.Factoid = xhr.response;

    } else {

      zAlert.show("NumbersAPI zRequest Succeeded", "./images/successIcon.png", code);
      controlState.Factoid = xhr.response;

    }

    control.syncState()

  });

};

window.onresize = function () {

  control.Wrapper.widthFix();
  control.alignText();
  zAlert.format();
  zAlert.hide();

};

control.alignText();

control.ideaData = [
  {
    title: 'Syntax',
    body: 'It\'s Javascript. A zFrame is an instantiable object that you instantiate like anything else (<code>var varName = new zFrame(\'idPrefix\')</code>). There aren\'t any weird constructions of arrays and objects required just to initialize a controller (or whatever you want to call it) without a terrifying exception being thrown.'
  },
  {
    title: 'Dependencies',
    body: 'If you need a script, load it in the template with a script tag, and it will be there. Reference its contents as normal. Additional layers of tweaky, fragile dependency injection with confusing (or non-existent) error reporting are not required.'
  },
  {
    title: 'Compilation',
    body: 'You shouldn\'t need to wash Javascript through a pre-processing step just to see one tiny little change you made. It\'s an unnecessary overcomplication.'
  },
  {
    title: 'HTML And The DOM',
    body: 'Templates and the functionality available on the objects declarable within them are powerful and readable. There\'s no need to redefine all that - a little extra convenience is all that\'s required.'
  },
  {
    title: 'Debugging',
    body: 'A developer ought to be able to debug their code normally and easily. You ought to be able to get at everything from the console, if necessary. Program execution ought to be traceable from top to bottom without a whole lot of fuss. ZFramework tries to be descriptive, non-obfuscated, and not minified. It also avoids wrapping or abstracting of objects, variables, and functions as much as is practicable.'
  },
  {
    title: 'Scoping',
    body: 'Related to debugging in general is scoping. Javascript already has plenty of ways to descriptively scope variables and objects, without having to resort to an abstraction like "$scope." You ought to be able to figure out what belongs to what just by reading your code and HTML, without the framework getting in the way or making you guess when things get complicated. You ought to be able to pass variables and objects around simply, and in a familiar way, instead of having to resort to framework-specific practices.'
  },
  {
    title: 'Digest Cycles',
    body: 'There isn\'t one. ZFramework runs updates to the DOM when it\'s been told to. If it hasn\'t been told to, it doesn\'t. You can certainly create event handlers to automate updates, but there\'s nothing being called endlessly by a window.setInterval() callback. You can use generic interval and timeout functionality without fear of messing something up, and no running digest cycle means that there\'s no digest cycle to unexpectedly and mysteriously stop.' 
  }
];

control.repeatTemplate(control.Idea, control.ideaData, function(element, index, idea) {
  element.IdeaTitle.innerHTML = idea.title;
  element.IdeaBody.innerHTML = idea.body;
  control.addElementsToThisZFrame(element);
});

control.Wrapper.widthFix();

control.attachUpdateEventsToInputs(control.validateInputs);
