# After you pull down this repository, simply open index.html in a browser for both the following information and documentation with examples.

## About ZFramework

*Simple, Uncluttered, No Bull Javascript For Getting Things Done*

ZFramework is an idea that grew out of a conversation between Nate McNeil and Danny Maland, back when they both worked for BlueDoor Software (Nate's company.) Nate began an initial, much more ambitious version, and Danny eventually ended up returning to a much more "cut down" interpretation of the idea.

ZFramework exists as an antidote to overweight, overwrought Javascript frameworks that insist on making simple tasks complicated...and complicated tasks nearly impenetrable. Angered by Angular? Riled by React? Vexed by Vue? Try this.

First, let's be fair. The big frameworks do things that zFramework just doesn't do, especially in the realm of template strings and custom "directives." You're not going to be putting something like {{variableName}} into your HTML and having that value auto-bind to a variable in your Javascript. You're not going to give a document element an attribute like zShow="variable", and have the element automatically hide when the variable evaluates as false. Those ARE handy behaviors, and if you really have to have them, zFramework isn't a good choice.

Let's also be honest, though. Other frameworks impose an enormous amount of overhead, both in code and training, in order to accomplish those things. When it takes a giant tutorial just to get started, when getting one little form built requires a complicated directory structure, when just getting things to display requires a "compilation" step...well, that's when Javascript has gone too far. Other frameworks abstract, rewrap, redefine, obfuscate, and even interfere with basic functionality and features that already work just fine - those features are just a bit inconvenient at times. The main point of zFramework is to increase the convenience of common DOM manipulation and data presentation tasks, while stil keeping all the basic mechanics from Javascript and standard DOM manipulation both intact and accessible.

## zFramework's Main Ideas

- Syntax: It's Javascript. A zFrame is an instantiable object that you instantiate like anything else (`var varName = new zFrame('idPrefix')`). There aren't any weird constructions of arrays and objects required just to initialize a controller (or whatever you want to call it) without a terrifying exception being thrown.

- Dependencies: If you need a script, load it in the template with a script tag, and it will be there. Reference its contents as normal. Additional layers of tweaky, fragile dependency injection with confusing (or non-existent) error reporting are not required.

- Compilation: You shouldn't need to wash Javascript through a pre-processing step just to see one tiny little change you made. It's an unnecessary overcomplication.

- HTML And The DOM: Templates and the functionality available on the objects declarable within them are powerful and readable. There's no need to redefine all that - a little extra convenience is all that's required.

- Debugging: A developer ought to be able to debug their code normally and easily. You ought to be able to get at everything from the console, if necessary. Program execution ought to be traceable from top to bottom without a whole lot of fuss. ZFramework tries to be descriptive, non-obfuscated, and not minified. It also avoids wrapping or abstracting of objects, variables, and functions as much as is practicable.

- Scoping: Related to debugging in general is scoping. Javascript already has plenty of ways to descriptively scope variables and objects, without having to resort to an abstraction like "$scope." You ought to be able to figure out what belongs to what just by reading your code and HTML, without the framework getting in the way or making you guess when things get complicated. You ought to be able to pass variables and objects around simply, and in a familiar way, instead of having to resort to framework-specific practices.

- Digest Cycles: There isn't one. ZFramework runs updates to the DOM when it's been told to. If it hasn't been told to, it doesn't. You can certainly create event handlers to automate updates, but there's nothing being called endlessly by a `window.setInterval()` callback. You can use generic interval and timeout functionality without fear of messing something up, and no running digest cycle means that there's no digest cycle to unexpectedly and mysteriously stop.
